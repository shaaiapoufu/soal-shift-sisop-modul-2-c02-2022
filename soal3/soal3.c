#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <wait.h>
#include <dirent.h>
#include <pwd.h>

void makefolder (char *folderName, char *projectPath){
    pid_t child = fork();
    if (child < 0) exit(EXIT_FAILURE);
    else if (child == 0){
        char folder[50];
        strcpy(folder, projectPath);
        strcat(folder, folderName);

        char *args[] = {"mkdir", "-p", folder, NULL};
        execv("/bin/mkdir", args);
    } else {
        wait(0);
    }
}

void unzip(char *fileName, char *projectPath){
    pid_t child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0){
        char filePath[150];
        strcpy(filePath,projectPath);
        strcat(filePath,fileName);

        char *args[] = {"unzip", filePath, "-d", projectPath, NULL};
        execv("/usr/bin/unzip",args);
    }else{
        wait(0);
    }
}

bool validate (char *fileName, char *key){
    if (strstr(fileName, key) != NULL) return true;
    return false;
}

void moveContent(char *folderName, char *projectPath, char *folderDest, char *key){
    pid_t child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0){
        DIR *dp;
        struct dirent *ep;
        
        char folderPath[150];
        strcpy(folderPath,projectPath);
        strcat(folderPath,folderName);
        strcat(folderPath,"/");

        dp = opendir(folderPath);

        if(dp != NULL){
            char folderDestPath[150];
            char list[1000][1000];
            strcpy(folderDestPath,projectPath);
            strcat(folderDestPath,folderDest);
            strcat(folderDestPath,"/");

            while ((ep = readdir (dp))){
                char filePath[150];
                strcpy(filePath,folderPath);
                strcat(filePath,ep->d_name);
                if (validate(ep->d_name, key)){
                    pid_t child2 = fork();
                    if(child2 < 0) exit(EXIT_FAILURE);
                    else if(child2 == 0){
                        char *args[] = {"mv",filePath,folderDestPath,NULL};
                        execv("/bin/mv",args);
                    }else{
                        wait(0);
                    }
                } 
            }
        }

    }else{
        int status;
        while((wait(&status)) > 0);
    }
}

void removeContent(char *folderName, char *projectPath, char *folderDest, char *key){
    pid_t child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0){
        DIR *dp;
        struct dirent *ep;
        
        char folderPath[150];
        strcpy(folderPath,projectPath);
        strcat(folderPath,folderName);
        strcat(folderPath,"/");

        dp = opendir(folderPath);

        if(dp != NULL){
            char folderDestPath[150];
            strcpy(folderDestPath,projectPath);
            strcat(folderDestPath,folderDest);
            strcat(folderDestPath,"/");

            while ((ep = readdir (dp))){
                char filePath[150];
                strcpy(filePath,folderPath);
                strcat(filePath,ep->d_name);
                if (validate(ep->d_name, key)){
                    pid_t child2 = fork();
                    if(child2 < 0) exit(EXIT_FAILURE);
                    else if(child2 == 0){
                        char *args[] = {"rm",filePath,NULL};
                        execv("/usr/bin/rm",args);
                    }else{
                        wait(0);
                    }
                } 
            }
        }

    }else{
        int status;
        while((wait(&status)) > 0);
    }
}

void removeDir (char *folderName, char *projectPath){
    pid_t child = fork();
    if (child < 0) exit(EXIT_FAILURE);
    else if (child == 0){
        char folder[50];
        strcpy(folder, projectPath);
        strcat(folder, folderName);

        char *args[] = {"rm", "-rf", folder, NULL};
        execv("/usr/bin/rm", args);
    } else {
        wait(0);
    }
}

void listAnimal(char *folderName, char *projectPath){
    char str[500], fileName[50];
    DIR *dp;
    struct dirent *ep;
    struct stat filePer;
    int r;
    char folderPath[150];
    strcpy(folderPath,projectPath);
    strcat(folderPath,folderName);
    strcat(folderPath,"/");
    strcpy(fileName, folderPath);
    strcat(fileName, "list.txt");

    dp = opendir(folderPath);    

    FILE *fp;
    fp = fopen(fileName, "w+");
    dp = opendir(folderPath);

    if(dp != NULL){
        while(ep = readdir(dp)){
            if(!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..") || !strcmp(ep->d_name,"list.txt")) continue;
            char getUser[100];
            strcpy(getUser, folderPath);
            strcat(getUser, ep->d_name);

            r = stat(getUser, &filePer);
            if(r == -1){
                fprintf(stderr, "File error\n");
                exit(1);
            }
            struct passwd *p = getpwuid(filePer.st_uid);

            char permission[10];
            strcpy(permission, "");
            strcat(permission, (filePer.st_mode & S_IRUSR) ? "r" : "-");
            strcat(permission, (filePer.st_mode & S_IWUSR) ? "w" : "-");
            strcat(permission, (filePer.st_mode & S_IXUSR) ? "x" : "-");

            char list[100];
            strcpy(list, p->pw_name);
            strcat(list, "_");
            strcat(list, permission);
            strcat(list, "_");
            strcat(list, ep->d_name);
            fprintf(fp, "%s\n", list);
        }
    } (void) closedir(dp);
}

int main(){
    pid_t child, childSID;

    child = fork();

    if (child < 0) exit(EXIT_FAILURE);
    else if (child > 0) exit(EXIT_SUCCESS);

    umask(0);
    childSID = setsid();

    if(childSID < 0) exit(EXIT_FAILURE);

    if((chdir("/")) < 0) exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    char projectPath[100], folderPath[100];
    strcpy(folderPath, "/home/elshervnn/");
    strcpy(projectPath, "/home/elshervnn/modul2/");

    pid_t child1 = fork();
    if (child1 < 0) exit(EXIT_FAILURE);
    else if (child1 == 0){
        makefolder("Darat/", projectPath);
    } else {
        wait(0);
        sleep(3);
        makefolder("Air/", projectPath);
        unzip("animal.zip", projectPath);
    }

    pid_t child2 = fork();
    if (child2 < 0) exit(EXIT_FAILURE);
    else if (child2 == 0){
        moveContent("animal", projectPath, "Air", "air");
    } else {
        moveContent("animal", projectPath, "Darat", "darat");
        wait(0);
        removeDir("animal", projectPath);
        removeContent("Darat", projectPath, "", "bird");

    }

    pid_t child3 = fork();
    if (child3 < 0) exit(EXIT_FAILURE);
    else if (child3 == 0){
        listAnimal("Air", projectPath);
    }

}