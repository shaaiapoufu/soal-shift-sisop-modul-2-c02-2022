#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <stdbool.h>
 
void delete_substr(char *str, char *substr){
    char *comp;
    int png = strlen(substr);
    while((comp = strstr(str, substr))){
        *comp = '\0';
        strcat(str, comp+png);
    }
}
 
void add_data(char title[200], char name[200], char year[200]){
    FILE *fl;
    char addPath[200];
    strcpy(addPath, "/home/maisan/shift2/drakor/");
    strcat(addPath, name);
    strcat(addPath, "/data.txt");
 
    fl = fopen(addPath, "a");
    fprintf(fl, "kategori: %s\n\nnama: %s\nrilis: tahun %s\n\n", name, title, year);
    fclose(fl);
}
 
void copy_image(char *fileName, char title[200], char name[200], char year[200], char path[]){
 
    char from[200];
    strcpy(from, path);
    strcat(from, fileName);
 
    delete_substr(fileName, ".png");
    char *token;
    token = strtok(fileName, "_;");
 
    int flag = 0;
    while(token != NULL){
        if(flag == 3){
            strcpy(title, token);
            
        }
 
        if(flag == 5){
            strcpy(name, token);
           
        }
 
 
        flag++;
        token = strtok(NULL, "_;");
    }
 
    char dest[200];
    strcpy(dest, "/home/maisan/shift2/drakor/");
    strcat(dest, name);
    strcat(dest, "/");
    strcat(dest, title);
    strcat(dest, ".png");
 
    pid_t ch = fork();
    int status;
 
    if(ch < 0) exit(EXIT_FAILURE);
 
    if(ch == 0){
        printf("%s --> %s\n", from, dest);
        char *argv[] = {"cp", from, dest, NULL};
        execv("/bin/cp", argv);
    }else{
        while((wait(&status)) > 0);
        add_data(title, name, year);
        return;
    }
 
}
 

 
void create_data(pid_t child_id){
    char cwd[200];
    getcwd(cwd, 200);
    char current[200];
 
    DIR *dir;
    struct dirent *dp;
    char path[200];
    char myPath[200];
    int status;
    int status2;
 
    strcpy(current, cwd);
 
    dir = opendir(current);
    strcpy(path, "/home/maisan/shift2/drakor/");
    strcpy(myPath, current);
    strcat(myPath, "/");
 
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 
            && strcmp(dp->d_name, "soal2") != 0 && strcmp(dp->d_name, "soal2.c") != 0) {
 
            char fileName[200];
            strcpy(fileName, dp->d_name);
 
            delete_substr(fileName, ".png");
 
            char name[200];
            char title[200];
            char year[200];
            char *split;
            split = strtok(fileName, "_;");
 
            int flag = 0;
            while(split != NULL){
 
                if(flag == 0 | flag == 3){
                    strcpy(title, split);
                }

                if(flag == 2 | flag == 5){
                    
                    strcpy(name, split);
                }

                if(flag == 1 | flag == 4){
                    strcpy(year, split);
                }

                if(strchr(dp->d_name, '/') == 0){
                    if(( strlen(title) > 0 & strlen(name) > 0 & strlen(year) > 0 )){
                        copy_image(dp->d_name, title, name, year, myPath);
                    }
                }
 
                split = strtok(NULL, "_;");
                flag++;
            }
        }
    }
 
    closedir(dir);
}
 
void delete_folder(pid_t child_id){
    char cwd[200];
    getcwd(cwd, 200);
 
    DIR *dir;
    struct dirent *dp;
    char path[200];
    char current[200];
    int status;
 
    strcpy(current, cwd);
 
    dir = opendir(current);
 
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dp->d_name);
 
            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                if(child_id == 0){
                    printf("%s\n", dp->d_name);
 
                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}
 
void duplicate_image(){
    char cwd[200];
    getcwd(cwd, 200);
    char current[200];
 
    DIR *dir;
    struct dirent *dp;
    char path[200];
    int status;
 
    strcpy(current, cwd);
    dir = opendir(current);
 
    strcpy(path, current);
    strcat(path, "/");
 
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && 
            strcmp(dp->d_name, "soal2") != 0 && strcmp(dp->d_name, "soal2.c") != 0){
 
            char fileName[200];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".png");
 
            char title[200];
            char name[200];
            char year[200];
 
            if(strchr(fileName, '_') != 0){
                char *token;
                token = strtok(fileName, "_;");
 
                int flag = 0;
                while(token != NULL){
                    if(flag == 0){
                        strcpy(title, token);
                    }
 
                    if(flag == 1){
                        strcpy(year, token);
                    }
 
                    if(flag == 2){
                        strcpy(name, token);
                    }
 
                    token = strtok(NULL, "_;");
                    flag++;
                }
 
        
            }else{continue;}
 
            strcat(path, dp->d_name);
 
            char dest[200];
            strcpy(dest, current);
            strcat(dest, "/");
            strcat(dest, title);
            strcat(dest, ";");
            strcat(dest, year);
            strcat(dest, ";");
            strcat(dest, name);
            strcat(dest, ".png");
 
 
            pid_t ch = fork();
            int status;
 
            if (ch < 0) exit(EXIT_FAILURE);
 
            if(ch == 0){
                char *argv[] = {"cp", path, dest, NULL};
                execv("/bin/cp", argv);
            }else{
                while((wait(&status)) > 0);
                ch = fork();
            }
        }
    }
    closedir(dir);
 
}
 
void create_genre(pid_t child_id){
    char cwd[200];
    getcwd(cwd, 200);
    char current[200];
 
    DIR *dir;
    struct dirent *dp;
    char path[200];
    char myDir[200];
    int status;
 
    strcpy(current, cwd);
 
    dir = opendir(current);
 
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
 
 
            if(child_id == 0){
                char fileName[200];
                strcpy(fileName, dp->d_name);
 
                delete_substr(fileName, ".png");
 
                char name[200];
                char *split;
                split = strtok(fileName, "_;");
 
                int flag = 0;
                while(split != NULL){
                    if(flag == 2){
                        strcpy(name, split);
                        int len = strlen(name);
                        if(len <= 8){
                            strcpy(myDir, "/home/maisan/shift2/drakor/");
                            strcat(myDir, name);
                        
 
                            char *argv[] = {"mkdir", "-p", myDir, NULL};
                            execv("/bin/mkdir", argv);
                        }
                    }
                    split = strtok(NULL, "_;");
                    flag++;
                }
            }
            while((wait(&status)) > 0);
            child_id = fork();
        }
    }
 
}
 
void create_folder(pid_t child_id){
    int status;
 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/maisan/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/home/maisan/sisop/modul2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }    
}
 
int main() {
    int status;
    pid_t child_id;
 
    child_id = fork();
 
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
 
    char cwd[200];
 
    if ((chdir(getcwd(cwd, 200))) < 0) {
        exit(EXIT_FAILURE);
    }
 
    if (child_id == 0){
        create_folder(fork());
       
    }else{
        while((wait(&status)) > 0);
 
        pid_t ch_del = fork();
        int statusDir;
 
        if(ch_del == 0){
            if (ch_del < 0) exit(EXIT_FAILURE);
            delete_folder(fork());
        }else{
            while((wait(&statusDir)) > 0);
 
            pid_t ch_gen = fork();
            int statusGen;
 
            
            if(ch_gen == 0){
                create_genre(fork());
            }else{
                while((wait(&statusGen)) > 0);
 
                pid_t ch_dup = fork();
                int statusDup;
 
                if(ch_dup < 0) exit(EXIT_FAILURE);
 
                if(ch_dup == 0){
                    duplicate_image();
                }else{
                    while((wait(&statusDup)) > 0);
 
                    pid_t ch_data = fork();
                    int statusData;
 
                    if(ch_data < 0) exit(EXIT_FAILURE);
 
                    if(ch_data == 0){
                        create_data(fork());
                    }
                }
            }
        }
    }
}
