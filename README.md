# Soal-Shift-Sisop-Modul-2-C02-2022
## Anggota Kelompok
| NRP        | Nama                         |
|------------|------------------------------|
| 5025201050 | Elshe Erviana Angely         |
| 5025201044 | Khariza Azmi Alfajira Hisyam |
| 5025201137 | Maisan Auliya                |
## Daftar Isi
- [Revisi]() <br />
- [Soal 1](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-2-c02-2022.git#soal-1) <br />
- [Soal 2](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-2-c02-2022.git#soal-2) <br />
- [Soal 3](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-2-c02-2022.git#soal-3) <br />
## Revisi
Pada saat demo kelompok kami revisi untuk problem 1.b, 1.c, 1.d, 1.e dan 2.c, 2.d, 2.e<br />
**Problem** <br />
- 1b. Menghitung gacha dengan random. Jumlah-gacha nya mod 10 maka akan membuat file baru (.txt) dan jika di mod 90, maka akan dibuat sebuah folder baru dan file `(.txt)`. Jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. 
- 1c. Menamai file dengan format nama `{Hh:Mm:Ss}_gacha_{jumlah-gacha}`
- 1d. Melakukan gacha dan menulis output dalam file `(.txt)`. Format penulisan adalah `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`
- 1e. Melakukan eksekusi dalam waktu tertentu.

**Penyelesaian**
```c
void read_file(char dir[], char filename[], GachaItem *character) 
{
    FILE *fp;
    char buffer[5120];
    char target_file[100];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    strcpy(target_file, dir);
    strcat(target_file, filename);

    fp = fopen(target_file, "r");
    fread(buffer, sizeof(char), 5120, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    // Parse a string and return a non-NULL json_object if a valid JSON value is found. 
    // The string does not need to be a JSON object or array; it can also be a string, 
    // number or boolean value.

    /*
    json_bool json_object_object_get_ex	(	struct json_object * 	obj,
                                            const char * 	key,
                                            struct json_object ** 	value	 
                                        )	 
    */
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    // Get the json_object associated with a given object field. This returns true if 
    // the key is found, false in all other cases (including if obj isn't a json_type_object).

    strcpy(character->name, json_object_get_string(name));
    character->rarity = json_object_get_int(rarity);
}

void parse_json(GachaItem items[], int type, int *count)
{
    DIR *dp;
    struct dirent *ep;

    char *path = (type == CHARACTER ? "gacha_gacha/characters/" : "gacha_gacha/weapons/");

    dp = opendir(path);

    if (dp != NULL)
    {
        for (int i = 0; (ep = readdir(dp)); i++)
        {
            GachaItem *item = malloc(sizeof(GachaItem));
            items[i] = *item;
            read_file(path, ep->d_name, &items[i]);

            *count = i;
        }

        (void)closedir(dp);
    }
}

void gacha(GachaItem items[], int *item_count, int *gacha_count, int primogems)
{
    struct tm *local;
    time_t t = time(NULL);

    local = localtime(&t);

    int random = rand() % (*item_count - 2);

    static char gacha_dir[1000];
    static char gacha_file[1500];
    char dir_count[15];
    char file_count[15];

    if (*gacha_count % 90 == 0)
    {
        sprintf(dir_count, "%d", *gacha_count + (primogems < 14400 ? primogems / 160 : 90));
        strcpy(gacha_dir, "gacha_gacha/total_gacha_");
        strcat(gacha_dir, dir_count);
        make_dir(gacha_dir);
    }

    if (*gacha_count % 10 == 0)
    {
        sprintf(file_count, "%d", *gacha_count + (primogems < 1600 ? primogems / 160 : 10));
        sprintf(gacha_file, "%s/%02d:%02d:%02d_gacha_%s.txt", gacha_dir, local->tm_hour, local->tm_min, local->tm_sec, file_count);
        sleep(1);
    }

    FILE *fp;
    fp = fopen(gacha_file, "a");
    fprintf(fp, "%d_%s_%s_%d\n", *gacha_count + 1, *gacha_count % 2 == 0 ? "character" : "weapon", items[random].name, items[random].rarity);
    fclose(fp);
}

void zip()
{
    char *arg[] = {"zip", "-r", "-qq", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
    run_exec("/usr/bin/zip", arg);
}

void remove_files(char dir[])
{
    char *arg[] = {"rm", "-rf", dir, NULL};
    run_exec("/bin/rm", arg);
}
```
Fungsi main untuk melakukan eksekusi dan memanggil fungsi yang sudah dibuat <br />
```c
while (1)
    {
        struct tm *local;
        time_t t = time(NULL);
        local = localtime(&t);

        if (local->tm_mday == 30 && local->tm_mon == 2 && local->tm_hour == 4 && local->tm_min == 44)
        {
            int primogems = 79000;

            GachaItem characters[50];
            GachaItem weapons[150];

            int *characters_count = malloc(sizeof(int));
            int *weapons_count = malloc(sizeof(int));
            int *gacha_count = malloc(sizeof(int));
            *gacha_count = 0;

            download("characters.zip", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp");
            download("weapon.zip", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT");
            make_dir("gacha_gacha");
            extract_zip("characters.zip", "gacha_gacha/");
            extract_zip("weapon.zip", "gacha_gacha/");

            parse_json(weapons, WEAPON, weapons_count);
            parse_json(characters, CHARACTER, characters_count);

            while (primogems >= 160)
            {
                if (*gacha_count % 2 == 0)
                {
                    gacha(characters, characters_count, gacha_count, primogems);
                }
                else
                {
                    gacha(weapons, weapons_count, gacha_count, primogems);
                }

                (*gacha_count)++;
                primogems -= 160;
            }
        }

        if (local->tm_mday == 30 && local->tm_mon == 2 && local->tm_hour == 7 && local->tm_min == 44)
        {
            zip();
            remove_files("gacha_gacha/");
            remove_files("characters.zip");
            remove_files("weapon.zip");
        }

        sleep(30);
    }
```

**Demo** <br />
![image](/uploads/4acfa8efd235b307d64a0af8c14ef4f2/image.png)

## Laporan Resmi
### Soal 1
#### Penjelasan
1. Mendownload file characters dan file weapons dari link yang sudah disediakan dan mengekstraknya. 

#### Penyelesaian
**Demo**
1. Mendownload file dan mengekstraknya
    ```
    void makefolder (char *folderName, char *projectPath){
    pid_t child = fork();
    if (child < 0) exit(EXIT_FAILURE);
    else if (child == 0){
        char folder[50];
        strcpy(folder, projectPath);
        strcat(folder, folderName);

        char *args[] = {"mkdir", "-p", folder, NULL};
        execv("/bin/mkdir", args);
        printf("folder %s created", folderName);
        } else {
            wait(0);
        }
    }

    ```
    ```
    void getzip (char *source, char *projectPath, char *fileName){
    pid_t child = fork();
        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0) {
            char filePath[150];
            strcpy(filePath, projectPath);
            strcat(filePath, fileName);

            char *args[] = {"wget", "--no-check-certificate", "-q", source, "-O", filePath, NULL};
            execv("/usr/bin/wget",args);
            printf("file zip downloaded");   
        } else {
            wait(0);
        }
    
    }
    ```
    void unzip(char *fileName, char *projectPath){
    pid_t child = fork();
        if(child < 0) exit(EXIT_FAILURE);
        else if(child == 0){
            char filePath[150];
            strcpy(filePath,projectPath);
            strcat(filePath,fileName);

            char *args[] = {"unzip", filePath, "-d", projectPath, NULL};
            execv("/usr/bin/unzip",args);
            printf("succesfully unzip");
        }else{
            wait(0);
        }
    }

    ```
    fungsi di main <br/>
    pid_t child1 = fork();
    if (child1 < 0) exit(EXIT_FAILURE);
    else if (child1 == 0){
        ...
        getzip("https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", workingDirectory, "characters.zip");
        unzip("characters.zip", workingDirectory);

        getzip("https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", workingDirectory, "weapon.zip");
        unzip("weapon.zip", workingDirectory);
    }

    ```
### Soal 2
#### Penjelasan
1. Mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`
2. Membuat folder untuk setiap jenis drama korea yang ada dalam zip

#### Penyelesaian
**Demo**
1. Mengextract zip dan menghapus folder 
    ```c
    void delete_folder(pid_t child_id){
        char cwd[200];
        getcwd(cwd, 200);
    
        DIR *dir;
        struct dirent *dp;
        char path[200];
        char current[200];
        int status;
    
        strcpy(current, cwd);
    
        dir = opendir(current);
    
        while((dp = readdir(dir)) != NULL){
            if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                
                strcpy(path, current);
                strcat(path, "/");
                strcat(path, dp->d_name);
    
                if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                    if(child_id == 0){
                        printf("%s\n", dp->d_name);
    
                        char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                        execv("/bin/rm", argv);
                    }
                    while((wait(&status)) > 0);
                    child_id = fork();
                }
            }
        }
    }
    ```
    ```

    ```

2. Membuat folder untuk drama korea yang ada di zip
    ```c
    void create_folder(pid_t child_id){
        int status;
        if (child_id == 0) {
            char *argv[] = {"mkdir", "-p", "/home/maisan/shift2/drakor", NULL};
            execv("/bin/mkdir", argv);
        }else{
            while((wait(&status)) > 0);
            char *argv[] = {"unzip", "/home/maisan/sisop/modul2/drakor.zip", NULL};
            execv("/bin/unzip", argv);
        }    
    }
    ```


### Soal 3
#### Penjelasan
1. Pada soal 3 a, kami diminta untuk membuat membuat membuat program untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat directory ke 2 dengan nama `air`,
2. Membuat program yang dapat melakukan extract `animal.zip` di `/home/[USER]/modul2/`.
3. Memisahkan hasil extract menjadi hewan darat dan hewan air sesuai dengan nama filenya. 
    - Hewan darat dimasukkan ke folder `/home/[USER]/modul2/darat` 
    - Hewan air dimasukkan ke folder `/home/[USER]/modul2/air`. 
    - Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. 
    - Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
4. Menghapus semua burung yang ada di directory `/home/[USER]/modul2/darat`. Hewan burung ditandai dengan adanya `bird` pada nama file.
5. Membuat file `list.txt` di folder `/home/[USER]/modul2/air`
    - Membuat list nama semua hewan yang ada di directory `/home/[USER]/modul2/air` ke `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]`.

#### Penyelesaian
**Demo**
1.  Membuat 2 direktori dengan membuat function `makefolder`
    Kode <br />
    ```c
    void makefolder (char *folderName, char *projectPath){
    pid_t child = fork();
        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char folder[50];
            strcpy(folder, projectPath);
            strcat(folder, folderName);

            char *args[] = {"mkdir", "-p", folder, NULL};
            execv("/bin/mkdir", args);
        } else {
            wait(0);
        }
    }
    ```
    ```c
    // memanggil fungsi di main
    pid_t child1 = fork();
        if (child1 < 0) exit(EXIT_FAILURE);
        else if (child1 == 0){
            makefolder("Darat/", projectPath);
    } else {
            ...
            makefolder("Air/", projectPath);
            ..
    }
                            
2. Mengextract `animal.zip`
    Kode <br />
    ```c
    void unzip(char *fileName, char *projectPath){
    pid_t child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0){
        char filePath[150];
        strcpy(filePath,projectPath);
        strcat(filePath,fileName);

        char *args[] = {"unzip", filePath, "-d", projectPath, NULL};
        execv("/usr/bin/unzip",args);
    }else{
        wait(0);
    }
    ```
    ```c
    // memanggil fungsi di main
    unzip("animal.zip", projectPath);
    ```

3. Memisahkan hasil extract menjadi hewan darat dan hewan air sesuai dengan nama filenya
    Kode <br />
    ```c
    // fungsi di main untuk membuat folder setelah 3 detik
    wait(0);
    sleep(3);
    ```
    ```c
    bool validate (char *fileName, char *key){
        if (strstr(fileName, key) != NULL) return true;
        return false;
    }

    void moveContent(char *folderName, char *projectPath, char *folderDest, char *key){
        pid_t child = fork();
        if(child < 0) exit(EXIT_FAILURE);
        else if(child == 0){
            DIR *dp;
            struct dirent *ep;
            
            char folderPath[150];
            strcpy(folderPath,projectPath);
            strcat(folderPath,folderName);
            strcat(folderPath,"/");

            dp = opendir(folderPath);

            if(dp != NULL){
                char folderDestPath[150];
                char list[1000][1000];
                strcpy(folderDestPath,projectPath);
                strcat(folderDestPath,folderDest);
                strcat(folderDestPath,"/");

                while ((ep = readdir (dp))){
                    char filePath[150];
                    strcpy(filePath,folderPath);
                    strcat(filePath,ep->d_name);
                    if (validate(ep->d_name, key)){
                        pid_t child2 = fork();
                        if(child2 < 0) exit(EXIT_FAILURE);
                        else if(child2 == 0){
                            char *args[] = {"mv",filePath,folderDestPath,NULL};
                            execv("/bin/mv",args);
                        }else{
                            wait(0);
                        }
                    } 
                }
            }

        }else{
            int status;
            while((wait(&status)) > 0);
        }
    }

    void removeDir (char *folderName, char *projectPath){
        pid_t child = fork();
        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char folder[50];
            strcpy(folder, projectPath);
            strcat(folder, folderName);

            char *args[] = {"rm", "-rf", folder, NULL};
            execv("/usr/bin/rm", args);
        } else {
            wait(0);
        }
    }

    //fungsi di main
    pid_t child2 = fork();
        if (child2 < 0) exit(EXIT_FAILURE);
        else if (child2 == 0){
            moveContent("animal", projectPath, "Air", "air");
        } else {
            moveContent("animal", projectPath, "Darat", "darat");
            wait(0);
            removeDir("animal", projectPath);
            ...

    }

    ```

4. Menghapus semua burung yang ada di directory `/home/[USER]/modul2/darat`
    Kode <br />
    ```c
    void removeContent(char *folderName, char *projectPath, char *folderDest, char *key){
    pid_t child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0){
        DIR *dp;
        struct dirent *ep;
        
        char folderPath[150];
        strcpy(folderPath,projectPath);
        strcat(folderPath,folderName);
        strcat(folderPath,"/");

        dp = opendir(folderPath);

            if(dp != NULL){
                char folderDestPath[150];
                strcpy(folderDestPath,projectPath);
                strcat(folderDestPath,folderDest);
                strcat(folderDestPath,"/");

                while ((ep = readdir (dp))){
                    char filePath[150];
                    strcpy(filePath,folderPath);
                    strcat(filePath,ep->d_name);
                    if (validate(ep->d_name, key)){
                        pid_t child2 = fork();
                        if(child2 < 0) exit(EXIT_FAILURE);
                        else if(child2 == 0){
                            char *args[] = {"rm",filePath,NULL};
                            execv("/usr/bin/rm",args);
                        }else{
                            wait(0);
                        }
                    } 
                }
            }

        }else{
            int status;
            while((wait(&status)) > 0);
        }
    }
    ```
    ```c
    // fungsi dipanggil di main
    removeContent("Darat", projectPath, "", "bird");
    ```

5. Membuat file `list.txt` di folder `/home/[USER]/modul2/air`
    Kode <br />
    ```c
    void listAnimal(char *folderName, char *projectPath){
    char str[500], fileName[50];
    DIR *dp;
    struct dirent *ep;
    struct stat filePer;
    int r;
    char folderPath[150];
    strcpy(folderPath,projectPath);
    strcat(folderPath,folderName);
    strcat(folderPath,"/");
    strcpy(fileName, folderPath);
    strcat(fileName, "list.txt");

    dp = opendir(folderPath);    

    FILE *fp;
    fp = fopen(fileName, "w+");
    dp = opendir(folderPath);

    if(dp != NULL){
        while(ep = readdir(dp)){
            if(!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..") || !strcmp(ep->d_name,"list.txt")) continue;
            char getUser[100];
            strcpy(getUser, folderPath);
            strcat(getUser, ep->d_name);

            r = stat(getUser, &filePer);
            if(r == -1){
                fprintf(stderr, "File error\n");
                exit(1);
            }
            struct passwd *p = getpwuid(filePer.st_uid);

            char permission[10];
            strcpy(permission, "");
            strcat(permission, (filePer.st_mode & S_IRUSR) ? "r" : "-");
            strcat(permission, (filePer.st_mode & S_IWUSR) ? "w" : "-");
            strcat(permission, (filePer.st_mode & S_IXUSR) ? "x" : "-");

            char list[100];
            strcpy(list, p->pw_name);
            strcat(list, "_");
            strcat(list, permission);
            strcat(list, "_");
            strcat(list, ep->d_name);
            fprintf(fp, "%s\n", list);
        }
        } (void) closedir(dp);
    }
    ```
    ```c
    // fungsi di main
    pid_t child3 = fork();
        if (child3 < 0) exit(EXIT_FAILURE);
        else if (child3 == 0){
            listAnimal("Air", projectPath);
        }

    ```
    **Demo** <br />
    ![soal3](/uploads/8296d41fc899c1fbcd702163db4cf7aa/image.png)
